// import Vue from "vue";
import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";
import * as VueGoogleMaps from "vue2-google-maps";

const firebaseConfig = {
  apiKey: "AIzaSyDsR-eZjOiGut5au10cGVsF9m-Vc1alJGI",
  authDomain: "cov-protect.firebaseapp.com",
  projectId: "cov-protect",
  storageBucket: "cov-protect.appspot.com",
  messagingSenderId: "713364598341",
  appId: "1:713364598341:web:9e841ba9d91a2fd7247d2c",
  measurementId: "G-WGEBG0QV9E"
};

firebase.initializeApp(firebaseConfig);
export default ({ Vue }) => {
  Vue.use(VueGoogleMaps, {
    load: {
      key: "AIzaSyBr5hFs0pirPs_by-lzxUsS7KXT_ZmwqD0"
      // libraries: 'places' // This is required if you use the Autocomplete plugin
      // OR: libraries: 'places,drawing'
      // OR: libraries: 'places,drawing,visualization'
      // (as you require)

      // If you want to set the version, you can do so:
      // v: '3.26',
    },

    // If you intend to programmatically custom event listener code
    // (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
    // instead of going through Vue templates (e.g. `<GmapMap @zoom_changed='someFunc'>`)
    // you might need to turn this on.
    // autobindAllEvents: false,

    // If you want to manually install components, e.g.
    // import {GmapMarker} from 'vue2-google-maps/src/components/marker'
    // Vue.component('GmapMarker', GmapMarker)
    // then set installComponents to 'false'.
    // If you want to automatically install all the components this property must be set to 'true':
    installComponents: true
  });
  Vue.prototype.$auth = firebase.auth();
  Vue.prototype.$db = firebase.firestore();
  Vue.prototype.$storage = firebase.storage();
};
