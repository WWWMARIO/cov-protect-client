import { Pie } from "vue-chartjs";
export default {
  /* watch: {
    update () {
      this.$data._chart.update();
    }
  }, */
  extends: Pie,
  props: ["pieChartData"/* , "update" */],
  mounted () {
    this.renderChart(this.pieChartData);
  }
};
