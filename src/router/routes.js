/*
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes */

const routes = [
  {
    path: "/Login",
    component: () => import("layouts/LoginPageLayout.vue"),
    children: [
      { path: "", component: () => import("pages/login/LoginIndex.vue") }
    ]
  },
  {
    path: "/welcome",
    component: () => import("layouts/WelcomePageLayout.vue"),
    children: [
      { path: "", component: () => import("pages/login/WelcomePageIndex.vue") }
    ]
  },
  {
    path: "/Administration",
    component: () => import("layouts/CovProtectSystemLayout.vue"),
    meta: { auth: true },
    children: [
      {
        path: "/Readings",
        meta: { auth: true },
        component: () => import("pages/CovProtectSystem/ReadingsIndex.vue")
      },
      {
        path: "/Stations",
        meta: { auth: true },
        component: () => import("pages/CovProtectSystem/StationsIndex.vue")
      },
      {
        path: "/home",
        meta: { auth: false },
        component: () => import("pages/Home.vue")
      }
    ]
  },
  //{
  //  path: "/Home",
  //  component: () => import("pages/Home.vue"),
  //  children: [
  //    { path: "", component: () => import("pages/Home.vue") }
  //  ]
  //},
];
// Always leave this as last one
// eslint-disable-next-line no-undef
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}
export default routes;
